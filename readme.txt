About: 
-------------
Tank is a networking game. The game idea is based on the well-known Battle City game with networking ability that allows multiple players play the game online.

Players share a signal game board and drive their own tank. The mission is to destroy all enemy tanks and defend your base from the enemy tank.

Author:
--------------
o Scott Wang (Server Side Programming)
o Terence Xuan (Client Side Programming)

Designation:
---------------
                    --------   fire   ------------
  --server control | Bullet |<-------| Enemy Tanks |
                    --------          ------------
                     ^   |                 | move
                     |   |  move           v 
                     |   |            --------------
                fire |   |---------->| Update Board |
               ----------------       --------------
              | Recieve player |           |
              |    input       |           v
               ---------------        ---------------
                     ^               | Send updated |
                     |               | board info   |
                     |                --------------
     Server Side     |                     |
    ---------------------------------------------------------  
     Client Side     |                     |
                     |                     |
                     |                     v
               ------------        --------------------
              | Send input |      | Recive update info |
               -----------         -------------------- 
                    ^                      |
                    |                      |
                    |                      v
               ------------           -------------
              | Get Input |          | Draw Screen |
               ----------             ------------
 
Implementation:
----------------
The game is implemented in two separate parts, GUI client and server. We will use Python as server-side programming language and C with OpenGL in client side.  
