/*****************************************
   Module: eheap.c
   Author: Terence Xuan
   Date:   2005 October 16

   Purpose: Allocate/free memory spaces.
*****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "eheap.h"

void *emalloc(size_t nbytes)
{
    void *p = malloc(nbytes);
    
    assert(p != NULL);
    
    return p;
}

void efree(void *p)
{
    assert(p != NULL);
    free(p);
}


