#ifndef TRUE
#define TRUE 1
#endif /* TRUE */

#ifndef FALSE
#define FALSE 0
#endif /* FALSE */


#define PROTOCOL_TYPE "tcp"

/* packet stuffing */
#define GLUE ';'
#define SUB_GLUE ':'
#define MOVE_IDEN "MOVE"
#define SHOOT_IDEN "SHOOT"

/* packet unstuffing*/
#define ID_IDEN "ID"
#define ENEMY_TANK 'E'
#define FRIEND_TANK 'T'
#define MISSILE     'B'

/* connection set up */
#define DEFAULT_HOST "192.168.1.107"
#define DEFAULT_PORT 6668

/*
#define DEFAULT_HOST "localhost"
#define DEFAULT_PORT 6666*/
