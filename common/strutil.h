/* Lab 3 Solution */
/* Bill Hohman */

#ifndef STRUTIL_H
#define STRUTIL_H

extern int getline(char s[], int max);
extern void chomp(char s[]);

extern void showwords(char **p);
extern char * trimleft(char *s);
extern int getwords(char *s, char sep, char *p[], int max);
extern char *mystrtok(char *s);

#endif
