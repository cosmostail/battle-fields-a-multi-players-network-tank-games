/* Lab 3 Solution */
/* Bill Hohman */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "strutil.h"

static int is_septor(char c, char sep);
void chomp(char s[])
{
    int len = strlen(s);
    
    if (len != 0) {
        if (s[len-1] == '\n')
            s[len-1] ='\0';
    }
}
        
/* getline: reads a line from stdin into s and returns length */
int getline(char s[], int max)
{
    int i, extra;
    char c;

    i = 0;
    c = getchar();
    if (c == EOF)
        return 0;
        
    while (i < max && c != '\n') {
        s[i] = c;
        i++;
        c = getchar();
    }
    
    if (c == '\n' && i < max) {
        s[i] = c;
        i++;
        extra = 0;
    } else
        extra = 1;
   
    s[i] = '\0';
    
    while (c != '\n') {
        c = getchar();
        extra++;
    }
    
    if (extra != 0)
        fprintf(stderr, "WARNING: %d characters were discarded\n", extra);

    return i;
}

char * trimleft(char *s)
{
  while (isspace((int) *s))
    s++;
  return s;
}

int getwords(char *s, char sep,  char *p[], int max)
{
  int i = 0, nextra = 0;
  char *tmp = s;

  s = trimleft(s);
    
  while (*s !='\0') {
    if (i < max) {
      p[i] = s;
      i++;
    } else {
      nextra++;
    }
    while (!is_septor(*s, sep) && *s != '\0')
      s++;
    if (*s != '\0') {
      *s = '\0';
      s++;
      s = trimleft(s);
    }
  }
    
  p[i] = NULL;

  s = tmp;
  return i;
}

void showwords(char **p)
{
    int i = 0;
    
    if (*p == NULL)
        printf("The list of words is empty\n");
    else {
        for ( ; *p != NULL; p++, i++)
            printf("Word[%2d] is --%s--\n", i, *p);
    }
}

char *mystrtok(char *s)
{
    static char *psave;
    
    if (s != NULL) 
	psave = trimleft(s);
    else
	psave = trimleft(psave);

    if (*psave == '\0')
	return NULL;
    else
	s = psave;    

    for (; !isspace((int) *psave) && *psave != '\0'; psave++)
	;

    if (*psave != '\0') {
        *psave = '\0';
        psave++;
    }

    return s;
}

int is_septor(char c, char sep)
{
  return (c == sep);
}
