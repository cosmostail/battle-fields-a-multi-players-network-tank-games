/*****************************************
   Module: eheap.h
   Author: Terence Xuan
   Date:   2005 October 16

   Purpose: Allocate/free memory spaces.
*****************************************/

#ifndef EHEAP_H
#define EHEAP_H

#include <stdio.h>

extern void *emalloc(size_t nbytes);

extern void efree(void *p);

#endif /* EHEAP_H */
