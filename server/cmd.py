################################################
# Module: cmd.py
# Author: Scott Wang
# Date:   April 20, 2007
# Purpose: deal players input
################################################
from string import *
from config import *

def deal_command(command_str,sockobj,global_lock,
                 players,enemys,bullets):
    action,x,y,arc =  split(split(command_str,GLUE)[0],SUB_GLUE)

    if action == "MOVE":
        global_lock.acquire()
        players[sockobj][2],players[sockobj][3],players[sockobj][4] = x,y,arc
        global_lock.release()
    elif action == "SHOOT":
        global_lock.acquire()
        bullets[sockobj] = ["B","999",x,y,arc]
        global_lock.release()

def quit(sockobj,readsocks,players):
    try:
        sockobj.close()
        del players[sockobj]
        readsocks.remove(sockobj)
    except:
        pass
