################################################
# Module: mystring.py
# Author: Scott Wang
# Date:   Jan 22, 2006
# Purpose: add some customer string utility
#          functions that doesn't provide by
#          python build in
################################################

def chomp(s):
    while (len(s) > 0 and s[-1] == '\n'):
        s = s[:-1]
    return s

