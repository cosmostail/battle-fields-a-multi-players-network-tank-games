################################################
# Module: myrandom.py
# Author: Scott Wang
# Date:   Oct 14, 2006
# Purpose: Generate a random instance that both
#          breaker and maker shared in order to
#          avoid them to make same random code
################################################

from random import *

#create a random instance shared by codemaker and breaker
r = Random()
