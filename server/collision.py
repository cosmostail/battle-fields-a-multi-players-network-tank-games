################################################
# Module: collision.py
# Author: Scott Wang
# Date:   April 20, 2007
# Purpose: collision detection between players
################################################
from config import *
from math import *

#return true iff they collision
def is_collision(object1,object2):
    x1,y1 = object1
    x2,y2 = object2
    
    x = float(x1)-float(x2)
    y = float(y1)-float(y2)
    return (sqrt(x*x + y*y) <= TANK_LENGTH)

def enemys_collision(enemys,new_pos,e_id):
    for enemy in enemys.values():
        OBJ,ID,x,y,arc = enemy
        if is_collision([x,y],new_pos) and ID != e_id:
            return True
    return False

def players_collision(players,new_pos):
    for sockobj, player in players.iteritems():
        OBJ,ID,x,y,arc = player
        if is_collision([x,y],new_pos):
            return sockobj
    
    return False

def bullets_collision(enemys,new_pos):
    for key,enemy in enemys.iteritems():
        OBJ,ID,x,y,arc = enemy
        if is_collision([x,y],new_pos):
            return key
    return None


def is_out_bond(x,y):
    return (x< MAX_LEFT or x > MAX_RIGHT or y < MAX_DOWN or y > MAX_UP)


#print is_collision(["0","0"],["20","0"])
#print is_collision(["0","0"],["25","0"])
#print is_collision(["0","0"],["10","0"])


