#! /usr/bin/python
################################################
# Module: server.py
# Author: Scott Wang
# Date:   Apirl 11, 2007
# Purpose: Tank is a networking game. The game idea
#          is based on the well-known Battle City game
#          with networking ability that allows multiple
#          players play the game online. Players share
#          a signal game board and drive their own tank.
#          The mission is to destroy all enemy tanks and
#          defend your base from the enemy tank.
################################################

from socket import socket,AF_INET,SOCK_STREAM
from select import select
import mystring
import string
import sys
import traceback
import thread
from config import *
from packet import *
from math import *
from cmd import *
from myrandom import *
from collision import *
import time

############################ Configure ############################ 
Host = ""
Port = 6666

######################## Server Initialize ########################
#a list of socks
mainsocks,readsocks,writesocks = [],[],[]

#a dict of players that using socket connection
#as a key, and a list with 3 elements as its value
#the values indicates
#[object,x,y,z,arc]

global players
global enemys
global bullets

players = {}
enemys = {}
bullets = {}

#create main socket connection
sockobj = socket(AF_INET, SOCK_STREAM)  
sockobj.bind((Host, Port))
sockobj.listen(1)

mainsocks.append(sockobj)
readsocks.append(sockobj)

#every objects has its own unique ID number
ID = "1"

# add a lock 
global_lock = thread.allocate_lock()

#calulate x 
def cal_x(player):
    Object,ID,x,y,arc = player
    x = float(x)
    arc = float(arc)
    return x + MOVING_DISTANCE * cos((degrees(270) - degrees(arc)) * PI/ degrees(STRAIGHT_ANGL))
    
#calculate y 
def cal_y(player):
    Object,ID,x,y,arc = player
    y = float(y)
    arc = float(arc)
    return y + MOVING_DISTANCE * sin((degrees(270) - degrees(arc)) * PI/ degrees(STRAIGHT_ANGL))


###### Create enemy force ########
for i in range(0,ENEMY_NUMBER):
    global_lock.acquire()
    enemys[i] = [ENEMY,ID,str(MAX_LEFT+TANK_LENGTH*i),str(MAX_UP),"0"]
    global_lock.release()
    ID = str(int(ID)+1)

############################ Thread ##############################
def enemys_move(global_lock,enemys):
    global players

    while(1):
        global_lock.acquire()
        for enemy in enemys.values():
            if(r.random() > 0.9):
                enemy[4] = str(r.randint(0,360))

            new_x = cal_x(enemy)
            new_y = cal_y(enemy)
            if (is_out_bond(new_x,new_y)
                   or enemys_collision(enemys,[new_x,new_y],enemy[1])):
                #force to change angel
                enemy[4] = str(r.randint(0,360))
            else:
                enemy[2] = str(new_x)
                enemy[3] = str(new_y)

                sockobj = players_collision(players,[new_x,new_y])
                if sockobj != False:
                    players[sockobj][2] = "0"
                    players[sockobj][3] = "0"
                
        global_lock.release()
        time.sleep(0.08)
        #print enemys

def bullets_move(global_lock):
    global enemys
    global bullets
    
    while(1):
        global_lock.acquire()
        for sockobj,bullet in bullets.iteritems():
            new_x = cal_x(bullet)
            new_y = cal_y(bullet)

            key = bullets_collision(enemys,[new_x,new_y])
            if key != None:
                enemys[key][2] = str(r.randint(MAX_LEFT,MAX_RIGHT))
                enemys[key][3] = str(r.randint(MAX_DOWN,MAX_UP))
                bullet[2] = "1024"
                bullet[3] = "1024"
            else:
                if is_out_bond(new_x,new_y):
                    bullet[2] = "1024"
                    bullet[3] = "1024"
                else:
                    bullet[2] = str(new_x)
                    bullet[3] = str(new_y)

            

        global_lock.release()
        time.sleep(0.05)


#create the enemys thread
thread.start_new_thread(enemys_move,(global_lock,enemys))

#create the bullet thread
thread.start_new_thread(bullets_move,(global_lock,))

#create send package thread
thread.start_new_thread(send_package,(global_lock,players,enemys,bullets))


############################ main loop ############################


while 1:
    try:
        readables,writeables,exceptions = select(readsocks,writesocks,[])
        for sockobj in readables:
            #it is a new connection
            if sockobj in mainsocks:
                newsock,address = sockobj.accept()
                readsocks.append(newsock)
                
                #estabilish players profile
                #[object,ID,x,y,z,arc]
                global_lock.acquire()
                players[newsock] = [TANK,ID,'0','0','0']
                global_lock.release()
                print "new connection found"
                newsock.send("ID"+SUB_GLUE+ID+GLUE)
                ID = str(int(ID) + 1)
            else:
                command_str = sockobj.recv(1024)
                command_str = mystring.chomp(command_str.strip())
                deal_command(command_str,sockobj,global_lock,
                             players,enemys,bullets)
               
    except KeyboardInterrupt:
        mainsocks[0].close()
        global_lock.release()
        raise
    except:
        quit(sockobj,readsocks,players)
        #global_lock.release()
        #traceback.print_exc()
        continue
    
