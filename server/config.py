################################################
# Module: config.py
# Author: Scott Wang
# Date:   April 19, 2007
# Purpose: Config file for tank games
################################################

#Object declear
TANK = "T"
BULLET = "B"
ENEMY = "E"

#Seprator
GLUE = ";"
SUB_GLUE = ":"

#length of TANK
TANK_LENGTH = 20

#map size
MAX_LEFT = -250
MAX_RIGHT = 250
MAX_UP = 250
MAX_DOWN = -250

#moving distance
MOVING_DISTANCE = 1.0
STRAIGHT_ANGL = 180.0
PI = 3.1415927

#max number of enemy
ENEMY_NUMBER = 5
