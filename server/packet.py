################################################
# Module: packet.py
# Author: Scott Wang
# Date:   Apirl 20, 2007
# Purpose: Generate a packet string to send
################################################

from socket import socket,AF_INET,SOCK_STREAM
from string import *
from config import *
import time

def send_package(global_lock,players,enemys,bullets):
    while(1):
        try:
            global_lock.acquire()
            message = ""
            for enemy in enemys.values():
                message += join(enemy,SUB_GLUE) + GLUE

            for player in players.values():
                message += join(player,SUB_GLUE) + GLUE

            for bullet in bullets.values():
                message += join(bullet,SUB_GLUE) + GLUE

            for socket in players.keys():
                socket.send(message)
            
            
            global_lock.release()
            time.sleep(0.05)
        except:
            global_lock.release()
            continue
