/*****************************************
   Module: tank_game.h
   Author: Terence Xuan
   Date:   2007 Feb 15th

   Purpose: application
*****************************************/
#ifndef TANK_GAME_H
#define TANK_GAME_H

typedef struct tank_game_s *TankGame;

extern TankGame tank_game_new();

extern void tank_game_init(TankGame game);

extern void tank_game_run(TankGame game);

extern void tank_game_end(TankGame game);

#endif /* TANK_GAME_H */
