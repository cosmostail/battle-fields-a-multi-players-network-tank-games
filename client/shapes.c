#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif /* __APPLE__ */

#include "shapes.h"

static GLint floor_size = 250;
static float hull_length = 6.0, hull_width = 4.0;
static float bridge_length = 2.0, bridge_width = 2.0;
static float gun_width = 0.1;
static float gun_length = 3.5, gun_angle = 5.0;
static float col_clear = 1.1, col_bright = 1.0, col_light = 0.9;
static float col_dark = 0.8, col_darker = 0.7;


/***************** solid objects *********************************/
void solid_floor()
{
  int i, j;

  glPushAttrib(GL_COLOR_BUFFER_BIT);

  for (i = -floor_size ; i <= floor_size; i+=floor_size/25) {
    for (j=-floor_size; j <= floor_size; j+=floor_size/25) {
      if ( (i+j) % 20 == 0) {
	glBegin(GL_POLYGON);
	glColor3f (0.75, 0.5, 0.95);
	glVertex3f(i-10, 0.0, j-10);
	glColor3f (0.09, 0.6, 0.8);
	glVertex3f(i-10, 0.0, j);
	glVertex3f(i, 0.0, j);
	glVertex3f(i, 0.0, j-10);
	glEnd();
      } else {
	glBegin(GL_POLYGON);
	glColor3f (0.2, 0.5, 0.2);
	glVertex3f(i-10, 0.0, j-10);
	glColor3f (0.09, 0.16, 0.8);
	glVertex3f(i-10, 0.0, j);
	glVertex3f(i, 0.0, j);
	glVertex3f(i, 0.0, j-10);
	glEnd();
      }  
    }
  }
  glPopAttrib();

}

void solid_tank(float r, float g, float b)
{
  glBegin(GL_QUADS);
  glPushAttrib(GL_COLOR_BUFFER_BIT);

  glColor3f (r*col_darker, g*col_darker, b*col_darker);

  glVertex3f ( -hull_width/2,  0.0,  -hull_length/2);
  glVertex3f ( -hull_width/2-0.1,  0.8,  -hull_length/2-0.1);
  glVertex3f ( hull_width/2+0.1,  0.8,  -hull_length/2-0.1);
  glVertex3f ( hull_width/2,  0.0,  -hull_length/2);

  glVertex3f ( hull_width/2,  0.0,  -hull_length/2);
  glVertex3f ( hull_width/2+0.1,  0.8,  -hull_length/2-0.1);
  glVertex3f ( hull_width/2+0.1,  0.8,  hull_length/2+0.1);
  glVertex3f ( hull_width/2,  0.0,  hull_length/2);

  glVertex3f ( hull_width/2,  0.0,  hull_length/2);
  glVertex3f ( hull_width/2+0.1,  0.8,  hull_length/2+0.1);
  glVertex3f ( -hull_width/2-0.1,  0.8,  hull_length/2+0.1);
  glVertex3f ( -hull_width/2,  0.0,  hull_length/2);

  glVertex3f ( -hull_width/2,  0.0,  hull_length/2);
  glVertex3f ( -hull_width/2-0.1,  0.8,  hull_length/2+0.1);
  glVertex3f ( -hull_width/2-0.1,  0.8,  -hull_length/2-0.1);
  glVertex3f ( -hull_width/2,  0.0,  -hull_length/2);


  glColor3f (r*col_light, g*col_light, b*col_light);

  glVertex3f ( -hull_width/2-0.1,  0.8,  -hull_length/2-0.1);
  glVertex3f ( -hull_width/2,  1.0,  -hull_length/2);
  glVertex3f ( hull_width/2,  1.0,  -hull_length/2);
  glVertex3f ( hull_width/2+0.1,  0.8,  -hull_length/2-0.1);

  glVertex3f ( hull_width/2+0.1,  0.8,  -hull_length/2-0.1);
  glVertex3f ( hull_width/2,  1.0,  -hull_length/2);
  glVertex3f ( hull_width/2,  1.0,  hull_length/2);
  glVertex3f ( hull_width/2+0.1,  0.8,  hull_length/2+0.1);

  glVertex3f ( hull_width/2+0.1,  0.8,  hull_length/2+0.1);
  glVertex3f ( hull_width/2,  1.0,  hull_length/2);
  glVertex3f ( -hull_width/2,  1.0,  hull_length/2);
  glVertex3f ( -hull_width/2-0.1,  0.8,  hull_length/2+0.1);


  glVertex3f ( -hull_width/2-0.1,  0.8,  hull_length/2+0.1);
  glVertex3f ( -hull_width/2,  1.0,  hull_length/2);
  glVertex3f ( -hull_width/2,  1.0,  -hull_length/2);
  glVertex3f ( -hull_width/2-0.1,  0.8,  -hull_length/2-0.1);


  glColor3f (r*col_bright, g*col_bright, b*col_bright);


  glVertex3f ( -hull_width/2,  1.0,  -hull_length/2);
  glVertex3f ( -hull_width/2,  1.0,  hull_length/2);
  glVertex3f ( hull_width/2,  1.0,  hull_length/2);
  glVertex3f ( hull_width/2,  1.0,  -hull_length/2);


  glPopAttrib();  
  glEnd();

  glBegin(GL_QUADS);
  glPushAttrib(GL_COLOR_BUFFER_BIT);

  /********************/
  /*     turret       */
  /********************/
  glColor3f (r*col_darker, g*col_darker, b*col_darker);

  glVertex3f ( -bridge_width/2,  1.0,  -bridge_length/2);
  glVertex3f ( -bridge_width/2-0.3,  1.3,  -bridge_length/2-0.5);
  glVertex3f ( bridge_width/2+0.3,  1.3,  -bridge_length/2-0.5);
  glVertex3f ( bridge_width/2,  1.0,  -bridge_length/2);

  glVertex3f ( bridge_width/2,  1.0,  -bridge_length/2);
  glVertex3f ( bridge_width/2+0.3,  1.3,  -bridge_length/2-0.5);
  glVertex3f ( bridge_width/2+0.3,  1.3,  bridge_length/2+0.5);
  glVertex3f ( bridge_width/2,  1.0,  bridge_length/2);

  glVertex3f ( bridge_width/2,  1.0,  bridge_length/2);
  glVertex3f ( bridge_width/2+0.3,  1.3,  bridge_length/2+0.5);
  glVertex3f ( -bridge_width/2-0.3,  1.3,  bridge_length/2+0.5);
  glVertex3f ( -bridge_width/2,  1.0,  bridge_length/2);

  glVertex3f ( -bridge_width/2,  1.0,  bridge_length/2);
  glVertex3f ( -bridge_width/2-0.3,  1.3,  bridge_length/2+0.5);
  glVertex3f ( -bridge_width/2-0.3,  1.3,  -bridge_length/2-0.5);
  glVertex3f ( -bridge_width/2,  1.0,  -bridge_length/2);


  glColor3f (r*col_light, g*col_light, b*col_light);

  glVertex3f ( -bridge_width/2-0.3,  1.3,  -bridge_length/2-0.5);
  glVertex3f ( -bridge_width/2,  2.1,  -bridge_length/2);
  glVertex3f ( bridge_width/2,  2.1,  -bridge_length/2);
  glVertex3f ( bridge_width/2+0.3,  1.3,  -bridge_length/2-0.5);

  glVertex3f ( bridge_width/2+0.3,  1.3,  -bridge_length/2-0.5);
  glVertex3f ( bridge_width/2,  2.1,  -bridge_length/2);
  glVertex3f ( bridge_width/2,  2.1,  bridge_length/2);
  glVertex3f ( bridge_width/2+0.3,  1.3,  bridge_length/2+0.5);

  glVertex3f ( bridge_width/2+0.3,  1.3,  bridge_length/2+0.5);
  glVertex3f ( bridge_width/2,  2.1,  bridge_length/2);
  glVertex3f ( -bridge_width/2,  2.1,  bridge_length/2);
  glVertex3f ( -bridge_width/2-0.3,  1.3,  bridge_length/2+0.5);

  glVertex3f ( -bridge_width/2-0.3,  1.3,  bridge_length/2+0.5);
  glVertex3f ( -bridge_width/2,  2.1,  bridge_length/2);
  glVertex3f ( -bridge_width/2,  2.1,  -bridge_length/2);
  glVertex3f ( -bridge_width/2-0.3,  1.3,  -bridge_length/2-0.5);

  glColor3f (r*col_bright, g*col_bright, b*col_bright);

  glVertex3f ( -bridge_width/2,  2.1,  -bridge_length/2);
  glVertex3f ( -bridge_width/2,  2.1,  bridge_length/2);
  glVertex3f ( bridge_width/2,  2.1,  bridge_length/2);
  glVertex3f ( bridge_width/2,  2.1,  -bridge_length/2);

  glPopAttrib();  
  glEnd();

  glPushAttrib (GL_COLOR_BUFFER_BIT);
  glColor3f (r*col_clear, g*col_clear, b*col_clear + 0.2);
  glBegin (GL_LINES);
  glVertex3f (-1.5, 1.0, 1.0);
  glVertex3f (-1.5, 3.5, 1.5);
  glEnd();
  glPopAttrib();

  glPushMatrix();
  glTranslatef(0.0005, 1.5, -0.5);
  glRotated(gun_angle, 1.0, 0.0, 0.0);
  glBegin(GL_QUADS);
  glPushAttrib(GL_COLOR_BUFFER_BIT);

  /*****************/
  /*   canon       */
  /*****************/


  glColor3f (r*col_dark, g*col_dark, b*col_dark);

  glVertex3f ( gun_width, 0.0, -gun_length);
  glVertex3f ( gun_width, 0.0, 0.0);
  glVertex3f ( 0.0, -gun_width, 0.0);
  glVertex3f ( 0.0, -gun_width, -gun_length);

  glVertex3f ( -gun_width, 0.0, 0.0);
  glVertex3f ( -gun_width, 0.0, -gun_length);
  glVertex3f ( 0.0, -gun_width, -gun_length);
  glVertex3f ( 0.0, -gun_width, 0.0);

  glColor3f (r*col_clear, g*col_clear, b*col_clear);
  glVertex3f ( 0.0, gun_width, -gun_length);
  glVertex3f ( 0.0, gun_width, 0.0);
  glVertex3f ( gun_width, 0.0, 0.0);
  glVertex3f ( gun_width, 0.0, -gun_length);

  glVertex3f ( 0.0, gun_width, 0.0);
  glVertex3f ( 0.0, gun_width, -gun_length);
  glVertex3f ( -gun_width, 0.0, -gun_length);
  glVertex3f ( -gun_width, 0.0, 0.0);

  glColor3f (r*col_darker, g*col_darker, b*col_darker);

  glVertex3f ( 0.0, gun_width, -gun_length);
  glVertex3f ( gun_width, 0.0, -gun_length);
  glVertex3f ( 0.0, -gun_width, -gun_length);
  glVertex3f ( -gun_width, 0.0, -gun_length);

  glPopAttrib();
  glEnd();

  glPopMatrix();
}

void solid_missile()
{
  GLUquadricObj *quadObj;
  GLdouble *sizeArray;
  GLfloat radius = 1.0;

  sizeArray = (GLdouble *) malloc (sizeof (GLdouble) * 1);
  *sizeArray = radius;

  glPushMatrix();
  glPushAttrib(GL_COLOR_BUFFER_BIT);

    glColor4f (0.1, 0.1, 0.1, 0.1);

    quadObj = gluNewQuadric ();
    gluQuadricDrawStyle (quadObj, GLU_FILL);
    gluQuadricNormals (quadObj, GLU_SMOOTH);
    gluSphere (quadObj, radius, 16, 16);

  glPopAttrib();
  glPopMatrix();
}
