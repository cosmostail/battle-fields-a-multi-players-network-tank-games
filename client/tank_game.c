/*****************************************
   Module: tank_game.c
   Author: Terence Xuan
   Date:   2007 Feb 15th

   Purpose: application
*****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "../common/eheap.h"

#include "network/global_network.h"
#include "network/packet.h"

#include "shared.h"
#include "global_client.h"
#include "tank_game.h"
#include "tank.h"
#include "tank_list.h"

#define UPDATE_MSEC 10.0
#define MAX_ZOOM 120.0
#define MIN_ZOOM 10.0

typedef struct tank_game_s {
  int pid;
  Tlist tlist;
} tank_game_t;

static GLdouble near_plane, far_plane;
static GLfloat aspect, zoom_factor;
static GLint width, height;
static int my_id;
static TKid my_tank;
static Tlist tank_list;
static int select_cam;
static int select_fog;
static int main_w;

/***********************************************************/
/*                private  functions                       */
/***********************************************************/

static void init();

/* time out function */
static void time_out();

/* display function */
static void display();

/* render world scene*/
static void draw_scene();

/* keyboard function */
static void keyboard_down(unsigned char key, int x, int y);

/* initialize game id */
static void wait_till_get_id(int *id);

/* unpack data from 'shared_read_buff' */
static void unpack_data();

/* deal with tank data */
static void unpack_data_tank(char *data_info[]);

/* deal with missile data */
static void unpack_data_missile(char *data_info[]);

/* pack data, and write to 'shared_write_buff' */
static void pack_data(char *iden, float *data, int seg);

/* camera setting */
static void set_cam(int cam_type);

/* game zooming */
static void set_zoom();
/***********************************************************/

TankGame tank_game_new()
{
  TankGame game = (TankGame)emalloc(sizeof (struct tank_game_s));
  return game;
}

void tank_game_init(TankGame game)
{
  assert(game != NULL);

  game->pid = -1;
  game->tlist = tlist_create();  
  
  /* wait till get game id */
  wait_till_get_id(&game->pid);

  /* make a local copy */
  my_id = game->pid;
  tank_list = game->tlist;
  my_tank = NULL;
}

void tank_game_run(TankGame game)
{
  assert(game != NULL);

  glutInit(shared_argc, shared_argv);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowSize(INIT_WIDTH, INIT_HEIGHT);
  glutInitWindowPosition(INIT_POS_X, INIT_POS_Y);

  /* main window */
  main_w = glutCreateWindow(APP_NAME);

  /* register keyboard handler */
  glutKeyboardFunc(keyboard_down);

  glutDisplayFunc(display);

  /* register timer function */
  glutTimerFunc(UPDATE_MSEC, time_out, 0);
  init();

  glutMainLoop();  
}

void tank_game_end(TankGame game)
{
  efree(game);
}


/***********************************************************/
/*                private  functions                       */
/***********************************************************/
void wait_till_get_id(int *id)
{
  char *seg[MAX_SEG];
  char *init_info[MAX_SUB_SEG];
  int num_seg;

  while(1) {
    pthread_mutex_lock(&mutex_reader);
    num_seg = packet_decode(shared_read_buff, GLUE, seg, MAX_SEG);  /* ID:12; */
    
    if(num_seg > 0) {         /* ignore empty packet */
      packet_decode(seg[0], SUB_GLUE, init_info, MAX_SUB_SEG); 
      if(strcmp(init_info[0], ID_IDEN) ==0)  {/* we have ID packet */
	*id = atoi(init_info[1]);
	packet_clear(shared_read_buff);
	pthread_mutex_unlock(&mutex_reader);
	break;
      }
    }
    packet_clear(shared_read_buff);
    pthread_mutex_unlock(&mutex_reader);
  }
}

void init()
{
  GLfloat fog_color[4] = {0.2, 0.2, 0.2, 1.0};

  near_plane = DEFAULT_NEAR;
  far_plane = DEFAULT_FAR;
  zoom_factor = DEFAULT_ZOOM;
  width = INIT_WIDTH;
  height = INIT_HEIGHT;

  /* cull face */
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);

  /* fog set up */
  glFogi(GL_FOG_MODE, GL_EXP);
  glFogfv(GL_FOG_COLOR, fog_color);
  glFogf(GL_FOG_DENSITY, 0.07);
  glHint(GL_FOG_HINT,GL_FASTEST);

  glShadeModel (GL_FLAT);

  glViewport( 0, 0, width, height );
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  aspect = (GLfloat) width / (GLfloat) height;
  gluPerspective( zoom_factor, aspect, near_plane, far_plane );
  glMatrixMode( GL_MODELVIEW );

  select_cam = DEFAULT_CAM;
  select_fog = FALSE;
}

void display()
{
  glClearColor (1.0, 1.0, 1.0, 1.0);
  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(0.0, 0.0, 0.0);
  
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  set_cam(select_cam);

  draw_scene();

  glutSwapBuffers();
  glFlush();
}

void draw_scene()
{
  /* fog */
  if (select_fog)
    glEnable(GL_FOG);
  else
    glDisable(GL_FOG);

  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glPushMatrix();

  /* tanks */
  unpack_data();
  tlist_draw_tank(tank_list);

  /* floor */
  glShadeModel (GL_SMOOTH);
  solid_floor();
  glShadeModel (GL_FLAT);

  glPopMatrix();
  glPopAttrib();
}

void keyboard_down(unsigned char key, int kx, int ky)
{
  TKid my_tank;
  my_tank = tlist_get_my_tank(tank_list, my_id);
  float data[3];
  float y;

  switch(key) {
  case QUIT:
    exit(0);
    break;
  case FORWARD:
    tkForward(my_tank, FORWARD_UNIT);
    break;
  case BACK:
    tkBack(my_tank, BACK_UNIT);
    break;
  case RIGHT:
    tkRight(my_tank, RIGHT_TURN_ANGLE);
    break;
  case LEFT:
    tkLeft(my_tank, LEFT_TURN_ANGLE);
    break;
  case FIRE:
    break;
  case KEY_CAM_FIRST:
    select_cam = CAM_FIRST;
    break;
  case KEY_CAM_SECOND:
    select_cam = CAM_SECOND;
    break;
  case KEY_CAM_BIRD:
    select_cam = CAM_BIRD;
    break;
  case FOG:
    select_fog = select_fog ? FALSE : TRUE ;
    break;
  case ZOOM_IN:
    zoom_factor = (zoom_factor <= MIN_ZOOM) ? MIN_ZOOM : zoom_factor - 5;
    set_zoom();
    break;
  case ZOOM_OUT:
    zoom_factor = (zoom_factor >= MAX_ZOOM) ? MAX_ZOOM : zoom_factor + 5;
    set_zoom();
    break; 
  }

  tkGetPos(my_tank, &data[0], &y, &data[1]);
  tkGetHeading(my_tank, &data[2]);

  /* since we moved our tank, let server know */
  if (key == FORWARD || key == BACK || key == LEFT || key == RIGHT) 
    pack_data(MOVE_IDEN, data, 3);
  else if (key == FIRE)
    pack_data(SHOOT_IDEN, data, 3);

}

void set_zoom()
{
  glViewport( 0, 0, width, height );
  glMatrixMode( GL_PROJECTION );
  glLoadIdentity();
  aspect = (GLfloat) width / (GLfloat) height;
  gluPerspective( zoom_factor, aspect, near_plane, far_plane );
  glMatrixMode( GL_MODELVIEW );

}
void time_out()
{
  display();
  glutTimerFunc(UPDATE_MSEC, time_out, 0);
}

void unpack_data()
{
  char *seg[MAX_SEG];
  char *data_info[MAX_SUB_SEG];
  int num_seg = 0;
  int i = 0;
  char k;

  //printf("%s\n", shared_read_buff);
  pthread_mutex_lock(&mutex_reader);
  num_seg = packet_decode(shared_read_buff, GLUE, seg, MAX_SEG);

  for (i = 0; i< num_seg; i++) {
    packet_decode(seg[i], SUB_GLUE, data_info, MAX_SUB_SEG);
    k = data_info[0][0];
    if (k == ENEMY_TANK || k == FRIEND_TANK) 
      unpack_data_tank(data_info);
    else if (k == MISSILE)
      unpack_data_missile(data_info);
  }

  packet_clear(shared_read_buff);
  pthread_mutex_unlock(&mutex_reader);
}

void unpack_data_tank(char *data_info[])
{
  char k;
  int id;
  float x,y,z,a;

  k = data_info[0][0];
  id = atoi(data_info[1]);
  x = atof(data_info[2]);
  y = 0.0;
  z = atof(data_info[3]);
  a = atof(data_info[4]);

  if(!tlist_tank_exist(tank_list, id)) {
    TKid t = tkNew();         /* if it's new tank, add it */
    tkInit(t,k,x,y,z,a);
    tlist_add(tank_list, t, id);
    if (my_id == id)
      my_tank = t;
  } else                      /* else update tank */
    tlist_update_tank(tank_list, id, x, y, z, a);
}

void unpack_data_missile(char *data_info[])
{
  int id;
  float x,y,z,a;

  id = atoi(data_info[1]);
  x = atof(data_info[2]);
  y = 0.0;
  z = atof(data_info[3]);
  a = atof(data_info[4]);

  tkDrawMissile(x, y, z, a);
}

void pack_data(char *iden, float *data, int seg)
{
  assert(data != NULL);
  char buffer[MAX_BUFFER];
  int i;

  strcpy(buffer, iden);
  i = strlen(iden);
  buffer[i++] = SUB_GLUE;
  buffer[i] = '\0';
  packet_encode(buffer, SUB_GLUE, data, seg);

  i = strlen(buffer);
  buffer[i++] = GLUE;
  buffer[i] = '\0';

  pthread_mutex_lock(&mutex_writer);
  strcat(shared_write_buff, buffer);
  pthread_mutex_unlock(&mutex_writer);
}


void set_cam(int cam_type)
{
  float x, y, z, a;
  x = y = z = a = 0.0;

  if (my_tank != NULL) {
    tkGetPos(my_tank, &x, &y, &z);
    tkGetHeading(my_tank, &a);
  }

  switch(cam_type) {
  case CAM_FIRST:
    gluLookAt(x, CAM_HEIGHT, z,
	      DES_X(x, a, 1.0), CAM_HEIGHT, DES_Z(z, a, 1.0),
	      0.0, 1.0, 0.0);
    break;
  case CAM_SECOND:
    gluLookAt(x, CAM_HEIGHT+2, z+20,
	      DES_X(x, a, 1.0), CAM_HEIGHT+2, DES_Z(z, a, 1.0),
	      0.0, 1.0, 0.0);
    break;
  case CAM_BIRD:
    gluLookAt(1, CAM_HEIGHT+400, 0.0,
	      0.0, 0.0, 0.0,
	      0.0, 1.0, 0.0);
    break;
  }

}
