/*****************************************
   Module: packet.c
   Author: Terence Xuan
   Date:   2007 Apri 10th

   Purpose: Packets handling

*****************************************/

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/socket.h>

/* common header */
#include "../../common/strutil.h"

/* local headers */
#include "packet.h"

#define MAX_FLOAT_LEN 20

int packet_decode(char *packet, char sep, char *seg[], int num_seg)
{
  assert(packet != NULL);
  return getwords(packet, sep, seg, num_seg);
}

extern void packet_encode(char *packet, char sep, float *data, int num_seg)
{
  assert(packet != NULL);

  char f[MAX_FLOAT_LEN];
  int i = 0, j;
  
  j = strlen(packet);

  for (i = 0; i < num_seg; i++) {
    sprintf(f, "%f", data[i]);
    j += strlen(f);
    strcat(packet, f);
    if(i < num_seg - 1)
      packet[j++] = sep;
    packet[j] = '\0';
  }
}

void packet_clear(char *packet)
{
  assert(packet != NULL);
  *packet = '\0';
}
