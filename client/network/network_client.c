/*****************************************
   Module: network_client.h
   Author: Terence Xuan
   Date:   2007 Apri 10th

   Purpose: Network connect, packet 
            receving and sending
*****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "../../common/global.h"
#include "../../common/eheap.h"
#include "../../common/strutil.h"

#include "global_network.h"
#include "network_client.h"

/* client shared global variables */
#include "../shared.h"

#define closesocket close

typedef struct network_client_s {
  char *host;
  int port;
  int sd;     //socket describer
} network_client_t;

NetworkClient network_client_new()
{
  NetworkClient network = (NetworkClient)emalloc(sizeof (struct network_client_s));
  return network;
}

void network_client_init(NetworkClient network, char *host, int port)
{
  assert (network != NULL && host != NULL);

  network->host = host;
  network->port = port;
  network->sd = 0;
}

int network_client_connect(NetworkClient network)
{
  char *host = network->host;
  int port = network->port;
  int sd;

  struct  hostent  *ptrh;  /* pointer to a host table entry       */
  struct  protoent *ptrp;  /* pointer to a protocol table entry   */
  struct  sockaddr_in sad; /* structure to hold an IP address     */

  memset((char *)&sad,0,sizeof(sad)); /* clear sockaddr structure */
  sad.sin_family = AF_INET;         /* set family to Internet     */

  if (port > 0)                   /* test for legal value         */
    sad.sin_port = htons((u_short)port);
  else {                          /* print error message and exit */
    fprintf(stderr,"bad port number %i\n",port);
    return FALSE;
  }

  /* Convert host name to equivalent IP address and copy to sad. */
  ptrh = gethostbyname(host);
  if ( ((char *)ptrh) == NULL ) {
    fprintf(stderr,"invalid host: %s\n", host);
    return FALSE;
  }
  memcpy(&sad.sin_addr, ptrh->h_addr, ptrh->h_length);

  /* Map TCP transport protocol name to protocol number. */
  if ( ((int)(ptrp = getprotobyname(PROTOCOL_TYPE))) == 0) {
    fprintf(stderr, "cannot map \"PROTOCOL_TYPE\" to protocol number");
    return FALSE;
  }

  /* Create a socket. */

  sd = socket(PF_INET, SOCK_STREAM, ptrp->p_proto);
  if (sd < 0) {
    fprintf(stderr, "socket creation failed\n");
    return FALSE;
  }

  /* Connect the socket to the specified server. */
  if (connect(sd, (struct sockaddr *)&sad, sizeof(sad)) < 0) {
    fprintf(stderr,"connect failed\n");
    return FALSE;
  }

  network->sd = sd;
  return TRUE;
}

void *network_client_recev(void *data)
{
  assert(data != NULL);

  NetworkClient network = (NetworkClient)data;

  int n = 0;
  char buffer[MAX_BUFFER];

  /* Repeatedly read data from socket and append to data */
  n = recv(network->sd, buffer, sizeof(buffer), 0);
  while (n > 0) {
    buffer[n] = '\0';

    pthread_mutex_lock(&mutex_reader);
      strcat(shared_read_buff, buffer);
    pthread_mutex_unlock(&mutex_reader);

    n = recv(network->sd, buffer, MAX_BUFFER, 0);
  }

  return NULL;
}

void *network_client_send(void *data)
{
  assert(data != NULL);

  NetworkClient network = (NetworkClient)data;  

  while (1) {
    pthread_mutex_lock(&mutex_writer);
    send(network->sd, shared_write_buff, strlen(shared_write_buff), 0);
    shared_write_buff[0] = '\0';
    pthread_mutex_unlock(&mutex_writer);    

  }
}

void network_client_close(NetworkClient network)
{
  /* Close the socket. */
  closesocket(network->sd);
}
