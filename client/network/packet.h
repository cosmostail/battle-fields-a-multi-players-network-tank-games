/*****************************************
   Module: packet.h
   Author: Terence Xuan
   Date:   2007 Apri 10th

   Purpose: Packets handling

*****************************************/

#ifndef PACKET_H
#define PACKET_H

/* decode packet into number of segements */
extern int packet_decode(char *packet, char sep, char *seg[], int num_seg);

/* encode number of segements into a packet */
extern void packet_encode(char *packet, char sep, float *data, int num_seg);

/* flush a packet */
extern void packet_clear(char *packet);

#endif
