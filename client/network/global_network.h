/*****************************************
   Module: network_global.h
   Author: Terence Xuan
   Date:   2007 Apri 10th

   Purpose: Define network buffer, packet
            size
*****************************************/

#ifndef GLOBAL_NETWORK_H
#define GLOBAL_NETWORK_H

#define MAX_BUFFER 512
#define MAX_DATA 2048
#define MAX_SEG 40
#define MAX_SUB_SEG 10

#endif /* GLOBAL_NETWORK_H */
