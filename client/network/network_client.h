/*****************************************
   Module: network_client.h
   Author: Terence Xuan
   Date:   2007 Apri 10th

   Purpose: Network connect, packet 
            receving and sending
*****************************************/

#ifndef NETWORK_CLIENT_H
#define NETWORK_CLIENT_H

typedef struct network_client_s *NetworkClient;

NetworkClient network_client_new();

/* initialize a network */
extern void network_client_init(NetworkClient network, char *host, int port);

/* connect to a network */
extern int network_client_connect(NetworkClient network);

/* reads in a socket describer, saves packets at where 'packet' pointer points 
   return number of character has been read */
extern void *network_client_recev(void *data);

/* send data */
extern void *network_client_send(void *data);

/* close network */
extern void network_client_close(NetworkClient network);
#endif
