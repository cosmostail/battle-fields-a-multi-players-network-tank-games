#include <stdio.h>
#include "shared.h"
#include "app_client.h"

int main(int argc, char **argv)
{
  shared_argc = &argc;
  shared_argv = argv;
  AppClient app = app_client_new();

  shared_init();
  if (app_client_init(app))
    app_client_run(app);

  app_client_end(app);
  shared_finit();
  printf("Thanks for playing\n");
  return 0;
}
