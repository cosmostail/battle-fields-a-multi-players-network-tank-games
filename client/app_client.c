/*****************************************
   Module: app_client.h
   Author: Terence Xuan
   Date:   2007 Feb 15th

   Purpose: application
*****************************************/

#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include "../common/global.h"
#include "../common/eheap.h"

#include "shared.h"
#include "global_client.h"
#include "app_client.h"
#include "tank_game.h"

#include "network/network_client.h"
#include "network/packet.h"

/***************************************/
/* local functions                     */
/***************************************/

/***************************************/


typedef struct app_client_s {
  TankGame game;
  NetworkClient network;
} app_client_t;

AppClient app_client_new()
{
  AppClient app = (AppClient)emalloc(sizeof (struct app_client_s));
  return app;
}

int app_client_init(AppClient app)
{
  assert(app != NULL);

  app->game = tank_game_new();
  app->network = network_client_new();

  /* initialize network parameters */
  if (*shared_argc > 2) 
    network_client_init(app->network, shared_argv[1], atoi(shared_argv[2]));
  else    
    network_client_init(app->network, DEFAULT_HOST, DEFAULT_PORT);

  /* connect to network */
  if (!network_client_connect(app->network))
    return FALSE;

  /* create receiver thread */
  if (pthread_create(&thrd_reader, NULL, network_client_recev, (void*)app->network))
    return FALSE;

  /* create sender thread */
  if (pthread_create(&thrd_writer, NULL, network_client_send, (void*)app->network))
    return FALSE;

  return TRUE;
}

void app_client_run(AppClient app)
{
  assert(app != NULL);

  tank_game_init(app->game);
  tank_game_run(app->game);
}

void app_client_end(AppClient app)
{
  assert(app != NULL);

  tank_game_end(app->game);
  network_client_close(app->network);

  efree(app);
}

