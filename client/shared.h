#include "../common/global.h"
#include "network/global_network.h"
#include <pthread.h>

extern int *shared_argc;
extern char **shared_argv;

/* read and write buffer shared between 
   network and application client */
extern char shared_read_buff[MAX_DATA];
extern char shared_write_buff[MAX_DATA];

/* reader, writer threads */
extern pthread_t thrd_reader;
extern pthread_t thrd_writer;

/* mutex for reader and writer */
extern pthread_mutex_t mutex_reader;
extern pthread_mutex_t mutex_writer;

extern void shared_init();
extern void shared_finit();

