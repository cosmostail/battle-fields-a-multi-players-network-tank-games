/*****************************************
   Module: tank_list.c
   Author: Terence Xuan
   Date:   2007 April 18

   Purpose: A linked list struct. Used to
            store created tanks on the
            screen.
*****************************************/
#include <math.h>
#include <assert.h>
#include "tank_list.h"
#include "../common/eheap.h"
#include "tank_list.h"

#define INV_ID -1

/* tank list struct */
typedef struct tank_list_s {
  TKid t;                    /* tank                    */
  int id;
  struct tank_list_s *prev;  /* pointer to previous node  */
  struct tank_list_s *next;  /* pointer to next node      */
} tank_list_t;

/************** local functions ****************/
/* insert a new node at the end of linked list */
static void tlist_insert_last(Tlist t_lit, Tlist node);

static Tlist tlist_find(Tlist t_list, int id);

/* return true if list is empty, return false otherwise */
static int tlist_empty(Tlist t_list);


/************** public functions **************/
Tlist tlist_create(void)
{
  Tlist t_list = (Tlist) emalloc(sizeof(struct tank_list_s));

  t_list->t = NULL;
  t_list->id = INV_ID;
  t_list->prev = NULL;
  t_list->next = NULL;

  return t_list;
}

void tlist_add(Tlist t_list, TKid t, int id)
{
  Tlist node = (Tlist) emalloc (sizeof(struct tank_list_s));

  node->t = t;
  node->id = id;
  node->prev = NULL;
  node->next = NULL;
  
  if ( t_list->next == NULL ) {         // if tank list is empty
    t_list->next = node;
    node->prev = t_list;
  } else                                // always insert tank at end of list
    tlist_insert_last(t_list, node);
}

void tlist_del(Tlist t_list, TKid t)
{
  assert(!tlist_empty(t_list) && t != NULL);
  Tlist tmp;

  for(tmp = t_list->next; tmp->t != t; tmp = tmp->next)
    ;

  if (tmp != NULL) {               /* if we find the node */
    printf("found\n");
    tmp->next->prev = tmp->prev;
    tmp->prev->next = tmp->next;
    tkDelete(tmp->t);             /* delete tank */
    efree(tmp);                   /* delete node */
  } else                          /* else we don't have the node */
    return;
}

void tlist_update_tank(Tlist t_list, int id, float x, float y, float z, float a)
{
  assert(t_list != NULL);
  /* make sure we can find the tank */
  Tlist found = tlist_find(t_list, id);
  assert(found != NULL);
    
  tkSetPos(found->t, x, y, z);
  tkSetHeading(found->t, a);
}

int tlist_tank_exist(Tlist t_list, int id)
{
  assert(t_list != NULL);
  Tlist tmp;
  
  tmp = tlist_find(t_list, id);

  return (tmp != NULL);
}

TKid tlist_get_my_tank(Tlist t_list, int id)
{
  assert(t_list != NULL);
  Tlist found;

  found = tlist_find(t_list, id);

  return (found->t);
}

void tlist_draw_tank(Tlist t_list)
{
  assert(t_list != NULL);
  Tlist current;

  for (current = t_list->next; current != NULL; current = current->next) {
    //printf("draw tank: %i\n", current->id);
    tkDrawTank(current->t);
  }

}

void tlist_dump(Tlist t_list)
{
  Tlist tmp;
  int i = 0;
  float x,y,z;

  for(tmp = t_list->next; tmp != NULL; tmp = tmp->next) {
    i++;
    tkGetPos(tmp->t, &x,&y, &z);
    printf("Tank(%d) at (%f,%f, %f)\n ", tmp->id, x,y,z);
  }

}

/*************** local functions **************/
void tlist_insert_last(Tlist t_list, Tlist node)
{
  assert(!tlist_empty(t_list));
  Tlist tmp;
  
  for (tmp = t_list->next; tmp->next != NULL; tmp = tmp->next)
    ;
  
  tmp->next = node;
  node->prev = tmp;

}

Tlist tlist_find(Tlist t_list, int id)
{
  assert(t_list != NULL);
  Tlist tmp;

  for (tmp = t_list; tmp != NULL; tmp = tmp->next)
    if (tmp->id == id)
      return tmp;
  
  return NULL;
}

int tlist_empty(Tlist t_list)
{
  return (t_list->next == NULL);
}
