/********************************************/
/* tank module modified from origin at:
http://www.step.polymtl.ca/~coyote/graphics_tank.html
*/
/*******************************************/
#ifndef SHAPES_H
#define SHAPES_H

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif /* __APPLE__ */

extern void solid_floor();
extern void solid_tank(float r, float g, float b);
extern void solid_missile();

#endif
