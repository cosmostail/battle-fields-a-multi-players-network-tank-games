/*****************************************
   Module: app_client.h
   Author: Terence Xuan
   Date:   2007 Feb 15th

   Purpose: application
*****************************************/
#ifndef APP_CLIENT_H
#define APP_CLIENT_H

typedef struct app_client_s *AppClient;

extern AppClient app_client_new();

/* initialize application:
   initialize networking,
   initialize tank game 
*/
extern int app_client_init(AppClient app);

/* run application:
   establish network connection,
   run tank game */
extern void app_client_run(AppClient app);

/* terminate application */
extern void app_client_end(AppClient app);

#endif /* APP_CLIENT_H */
