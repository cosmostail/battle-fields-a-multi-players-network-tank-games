/*****************************************
   Module: tank_list.h
   Author: Terence Xuan
   Date:   2007 April 18

   Purpose: A linked list struct. Used to
            store created tanks on the
            screen.
*****************************************/

#ifndef TANK_LIST_H
#define TANK_LIST_H

#include "../common/global.h"

#include "tank.h"

typedef struct tank_list_s *Tlist;

/* creat a empty tank list */
extern Tlist tlist_create(void);

/* add a tank to list */
extern void tlist_add(Tlist t_list, TKid t, int id);

/* update a tank in the list */
extern void tlist_update_tank(Tlist t_list, int id, float x, float y, 
			      float z, float a);

/* delete a turtle in the list */
extern void tlist_del(Tlist t_list, TKid t);

/* return true if a tank exits, 
   return false otherwise */
extern int tlist_tank_exist(Tlist t_list, int id);

extern TKid tlist_get_my_tank(Tlist t_list, int id);
extern void tlist_draw_tank(Tlist t_list);

/* print all turtle's coordinate */
extern void tlist_dump(Tlist t_list);

#endif /* TANK_LIST_H */
