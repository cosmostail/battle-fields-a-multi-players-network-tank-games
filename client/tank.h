/*****************************************
   Module: tank.h
   Author: Terence Xuan
   Date:   2007 Feb 15th
           Modified: April 15th

   Purpose: Tank library
*****************************************/
#ifndef TANK_H
#define TANK_H

#include "../common/global.h"
#include "shapes.h"

/****************** Constants ********************/
#define TK_NO_TANK    NULL

/******************** Data Types *****************/
typedef float                    TKfloat;
typedef int                      TKbool;
typedef char                     TKkind;
typedef struct tank_s            *TKid;

/************ useful math macro and constants for tank library ************/
#define RIGHT_ANGLE 90.0
#define STRAIGHT_ANGLE 180.0
#define CIRCLE_ANGLE  360.0
#define MAX_MISSILE_DISTANCE 500
#define DEFAULT_ANGLE 270

/* calculate coordinate, moving point (x,y) 
   to distance 'd' with angle 'a'
*/
#define DES_X(x,a,d) (x + d * cos((DEFAULT_ANGLE - a) * M_PI / STRAIGHT_ANGLE))
#define DES_Z(z,a,d) (z + d * sin((DEFAULT_ANGLE - a) * M_PI / STRAIGHT_ANGLE))


/******************* Public Functions ********************/
/* tank management */
/* creat a new tank */
extern TKid tkNew();

extern void tkInit(TKid t, TKkind k, TKfloat x, TKfloat y, TKfloat z, TKfloat a);

/* delete a tank */
extern void tkDelete(TKid t);

/* tank movement */
extern void tkLeft(TKid t, TKfloat a);

extern void tkRight(TKid t, TKfloat a);

extern void tkForward(TKid t, TKfloat d);

extern void tkBack(TKid t, TKfloat d);

extern void tkSetPos(TKid t, TKfloat x, TKfloat y, TKfloat z);

extern void tkSetHeading(TKid t, TKfloat a);

extern void tkGetPos(TKid t, TKfloat *x, TKfloat *y, TKfloat *z);

extern void tkGetHeading(TKid t, TKfloat *a);

/* fire a missile */
extern void tkFire(TKid t);

extern void tkDrawMissile(float x, float y, float z, float a);

/* return true if missile hits an enemy, return 0 otherwise */
extern int tkCheckHit(TKid t);

/* draw tank, and missile if it is fired */
extern void tkDrawTank(TKid t);
#endif /* TANK_H */


