/*****************************************
   Module: tank.c
   Author: Terence Xuan
   Date:   2007 Feb 15

   Purpose: Tank library
*****************************************/
#include <stdio.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif /* __APPLE__ */

#include <assert.h>
#include <math.h>
#include "tank.h"
#include "shapes.h"
#include "../common/eheap.h"
#include "../common/global.h"

#define DEFAULT_TANK_SIZE 1

/***************************************/
/*  Data Types */
/***************************************/
typedef struct tank_m_s *TKmissile;

/* tank struct */
typedef struct tank_s{
  TKfloat       x;              /* current coordinates */
  TKfloat       y;
  TKfloat       z;
  TKfloat       a;              /* angle       */
  TKkind        k;
  TKmissile     m;             /* missile     */
} tank_t;

/* tank's missile */
typedef struct tank_m_s{
  TKfloat x;
  TKfloat y;
  TKfloat z;
  TKfloat a;
  TKfloat d;             /* missile remaining distance */
  TKbool  fired;         /* if missile is fired */
} tank_m_t;

/*****************************************/
/*             local function            */
/*****************************************/
/* reset missile */
static void tkResetMissile (TKid t); 

/********** Public Functions ************/
/* tank management */
TKid tkNew()
{
  TKid t = (TKid) emalloc(sizeof(struct tank_s));
  return t;
}

void tkInit(TKid t, TKkind k, TKfloat x, TKfloat y, TKfloat z, TKfloat a)
{
  t->m = (TKmissile) emalloc(sizeof(struct tank_m_s));

  t->k = k;
  t->x = x;              /* x axis      */
  t->y = y;              /* y axis      */
  t->z = z;              /* z axis      */
  t->a = a;              /* angle       */
  t->m->d = 0.0;
  t->m->fired = FALSE;
}

void tkDelete(TKid t)
{
  efree(t->m);
  efree(t);
}

/* tank movement */
void tkLeft(TKid t, TKfloat a)
{
  assert(t != TK_NO_TANK);

  tkSetHeading(t, t->a + a);
}

void tkRight(TKid t, TKfloat a)
{
  assert(t != TK_NO_TANK);

  tkSetHeading(t, t->a - a);
}

void tkForward(TKid t, TKfloat d)
{
  assert(t != TK_NO_TANK);

  t->x = DES_X(t->x, t->a, d);
  t->z = DES_Z(t->z, t->a, d);
}

void tkBack(TKid t, TKfloat d)
{
  tkForward(t, -d);
}

void tkSetPos(TKid t, TKfloat x, TKfloat y, TKfloat z)
{
  assert(t != TK_NO_TANK);

  t->x = x;
  t->y = y;
  t->z = z;
}

void tkSetHeading(TKid t, TKfloat a)
{
  assert(t != TK_NO_TANK);

  t->a = a;
}

void tkGetPos(TKid t, TKfloat *x, TKfloat *y, TKfloat *z)
{
  assert(t != TK_NO_TANK);
  
  *x = t->x;
  *y = t->y;
  *z = t->z;
}

void tkGetHeading(TKid t, TKfloat *a)
{
  assert(t != TK_NO_TANK);

  *a = t->a;
}

void tkFire(TKid t)
{

}

void tkDrawTank(TKid t)
{
  assert(t != TK_NO_TANK);

  glPushMatrix();
  glTranslatef(t->x,t->y,t->z);
  glRotatef(t->a, 0.0, 1.0, 0.0);

  if (t->k == FRIEND_TANK)
    solid_tank(0.2, 0.8, 0.2);
  else
    solid_tank(0.8, 0.2, 0.2);    
  glPopMatrix();

}

void tkDrawMissile(float x, float y, float z, float a)
{
  
  /* draw missile */
  glPushMatrix();  
  glTranslatef(x, y+1, z);
  glRotatef(a, 0.0, 1.0, 0.0);
  solid_missile();
  glPopMatrix();

}

void tkResetMissile(TKid t)
{
  assert(t != TK_NO_TANK);

  t->m->x = 0.0;
  t->m->y = 0.0;
  t->m->z = 0.0;
  t->m->a = 0.0;
  t->m->d = 0.0;
  t->m->fired = FALSE;
}
