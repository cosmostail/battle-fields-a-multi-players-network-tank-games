#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif /* __APPLE__ */

#define APP_NAME "3D Tank"

#define INIT_WIDTH 1024
#define INIT_HEIGHT 768

#define INIT_POS_X 0
#define INIT_POS_Y 0

/* projection settings */
#define DEFAULT_NEAR 0.1
#define DEFAULT_FAR  1000.0
#define DEFAULT_ZOOM 60.0

/* camaer */
#define CAM_FIRST  0
#define CAM_SECOND 1
#define CAM_BIRD   2

/* game settings */
#define CAM_HEIGHT 2
#define DEFAULT_CAM CAM_FIRST

/* key control */
#define QUIT 27 /* ESC key */
#define FORWARD 'w'
#define BACK    's'
#define RIGHT   'd'
#define LEFT    'a'
#define FIRE    ' '
#define KEY_CAM_FIRST  '1'
#define KEY_CAM_SECOND '2'
#define KEY_CAM_BIRD   '3'
#define FOG     'f'
#define ZOOM_IN  '='
#define ZOOM_OUT '-'

/* tank movement setting */
#define FORWARD_UNIT 0.5
#define BACK_UNIT 0.5
#define RIGHT_TURN_ANGLE 1.0
#define LEFT_TURN_ANGLE RIGHT_TURN_ANGLE
