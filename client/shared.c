#include "shared.h"

int *shared_argc;
char **shared_argv;

/* read and write buffer shared between 
   network and application client */
char shared_read_buff[MAX_DATA];
char shared_write_buff[MAX_DATA];

/* reader, writer threads */
pthread_t thrd_reader;
pthread_t thrd_writer;

/* mutex for reader and writer */
pthread_mutex_t mutex_reader;
pthread_mutex_t mutex_writer;

void shared_init()
{
  pthread_mutex_init(&mutex_reader, NULL);
  pthread_mutex_init(&mutex_writer, NULL);
}

void shared_finit()
{
  pthread_mutex_destroy(&mutex_reader);
  pthread_mutex_destroy(&mutex_writer);
  //pthread_cancel(thrd_reader);
  //pthread_cancel(thrd_writer);
}
